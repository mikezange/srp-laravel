<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $N = "94ce8df220e993be63d9f7a31d566fa5f840e9d146e61fc4ec17e335ef6ebb4b502946459b4ab6c72e309218a4ff6be2050c0aa5d41f61c7f849c024e81ddc224ef51f2caedd1f331584e62673afd87288692cf4cdaa487ce2a55517ab3674ddf664d8c89993ca5b0ae81cd5e36099d8c279dcde7f1ccf9b5c810384c116ca31ee904d87ce32e68159628ff123220326fde409c7c1f59283db1a596456a424322945c055416c045efcadf25e742027f499af5c0871d2189515409f8d7270e2e74d14f943296ce0ef44c94e1366285d6a41a69a9ef2011af87f7654c2b7d2346b9858f186eaa868e85d64266596c7a7fe5993843ecba013e2530c48387414de1f";
    protected $g = "2";
    protected $algo = "sha256";
    protected $k;

    public function index(Request $request)
    {
        return view('welcome');
    }

    public function register(Request $request)
    {
        $exists = User::where(['username' => $request->input('username')])->first();

        if ($exists) {
            $data = [
                'status' => 'error',
                'reason' => 'User exists'
            ];

            return json_encode($data);
        }

        $user = User::create($request->input());

        if ($user) {
            $data = [
                'status' => 'success',
                'reason' => 'User created'
            ];

            return json_encode($data);
        }

        $data = [
            'status' => 'error',
            'reason' => 'User could not be created'
        ];

        return json_encode($data);
    }

    public function login(Request $request)
    {
        $data = $request->input();

        $this->k = hash($this->algo, $this->N . $this->g);

        if ($data['phase'] === 1) {
            $user = User::where('username', $data['I'])->first();

            $A = $this->hex2dec($data['A']);
            $N = $this->hex2dec($this->N);

            if (bccomp($A, 0, 0) === 0 || !bcmod($A, $N)) {
                dd('bad A number from the client');
            }

            $b = null;

            try {
                $b = bin2hex(random_bytes(30));
            } catch (\Exception $e) {
                dd("couldn't generate secure random");
            }

            $B = $this->generateB($b, $user->verifier);

            $return = [
                'status' => true,
                'B' => $B,
                's' => $user->salt,
            ];

            session()->put('login', [
                'I' => $user->username,
                'u' => $this->generateU($data['A'], $B),
                'A' => $data['A'],
                'B' => $B,
                'b' => $b,
            ]);

            return json_encode($return);
        } elseif ($data['phase'] === 2) {
            $session = session()->get('login');
            $user = User::where('username', $session['I'])->first();

            if ($user) {
                $S = $this->generateS($user, $session);
                $K = hash($this->algo, $S);
                $M = hash($this->algo, $K.$session['A'].$session['B'].$user->username.$user->salt.$this->N.$this->g);

                if ($data['M'] !== $M) {
                    $return = [
                        'status' => false,
                        'Z' => "Not valid",
                    ];
                    return json_encode($return);
                }

                $Z = hash($this->algo, $session['A'] . $M . $K);

                $return = [
                    'status' => true,
                    'Z' => $Z,
                ];
                return json_encode($return);
            }
        }
    }

    protected function getCharacters($base)
    {
        $characters = "";

        if ($base > 64) {
            for ($x = 0; $x < 256; $x++) {
                $characters .= chr($x);
            }
        } else {
            $characters .= "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_";
        }

        $characters = substr($characters, 0, $base);

        return (string)$characters;
    }

    private function generateB($b, $v)
    {
        $n = $this->hex2dec($this->N);
        $k = $this->hex2dec($this->k);
        $b = $this->hex2dec($b);
        $v = $this->hex2dec($v);

        $B = gmp_add(gmp_mul($k, $v), gmp_powm($this->g, $b, $n));

        return $this->dec2Hex($B);
    }

    private function generateU($A, $B)
    {
        return hash($this->algo, $A.$B);
    }

    private function generateS($user, $data)
    {
        $v = $this->hex2dec($user->verifier);
        $A = $this->hex2dec($data['A']);
        $u = $this->hex2dec($data['u']);
        $b = $this->hex2dec($data['b']);
        $N = $this->hex2dec($this->N);

        $S = gmp_powm(gmp_mul($A, gmp_powm($v, $u, $N)), $b, $N);

        return $this->dec2Hex($S);
    }

    public function dec2Hex($dec)
    {
        $hex = "";

        while ($dec > 0) {
            $last = gmp_intval(gmp_mod($dec, 16));
            $hex = dechex($last).$hex;
            $dec = gmp_div(gmp_sub($dec, $last), 16);
        }

        return $hex;
    }

    public function hex2dec($hex)
    {
        $chars = "0123456789abcdef";
        $length = strlen($hex);
        $result = '';
        $number = [];

        for ($i = 0; $i < $length; $i++) {
            $number[$i] = strpos($chars, $hex{$i});
        }

        do {
            $divide = 0;
            $newLen = 0;
            for ($i = 0; $i < $length; $i++) {
                $divide = $divide * 16 + $number[$i];
                if ($divide >= 10) {
                    $number[$newLen++] = (int)($divide / 10);
                    $divide = $divide % 10;
                } elseif ($newLen > 0) {
                    $number[$newLen++] = 0;
                }
            }
            $length = $newLen;
            $result = $chars{$divide} . $result;
        } while ($newLen != 0);
        return $result;
    }
}
