Requires: Valet, PHP7, yarn

- clone
- composer install
- yarn
- yarn dev
- copy .env.example > .env
- php artisan key:generate
- create ./database/database.sqlite
- php artisan migrate


- valet link srplaravel
- valet secure srplaravel

visit https://srplaravel.test/

Check the console.