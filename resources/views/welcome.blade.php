<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <script src="{{ mix('js/app.js')  }}"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div>
                    Register:
                    <form method="post">
                        <input name="username" class="username_register" value="mike" type="text" placeholder="username">
                        <input name="password" class="password_register" value="test" type="password" placeholder="password">
                        <button class="register">Register</button>
                    </form>
                </div>
                <br><br>
                <div>
                    Login:
                    <form method="post">
                        <input name="username" class="username_login" value="mike" type="text" placeholder="username">
                        <input name="password" class="password_login" value="test" type="text" placeholder="password">
                        <button class="login">Login</button>
                    </form>
                </div>

            </div>
        </div>
    <script>
      $(".login").click(function(e){
        e.preventDefault();
        srp.login($('.username_login').val(), $('.password_login').val());
        return false;
      });

      $(".register").click(function(e){
        e.preventDefault();
        srp.register($('.username_register').val(), $('.password_register').val());
        return false;
      })
    </script>
    </body>
</html>
