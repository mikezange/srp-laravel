
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

const crypt = require("crypto");

import { add, bigInt2str, str2bigInt, powMod, mult, sub } from 'leemon'

window.crypter = {
    encrypt: (passphrase, plaintext) => {
        const iv = crypto.getRandomValues(new Uint8Array(12));
        const data = window.crypter.str2buf(plaintext);
        return window.crypter.deriveKey(passphrase).then(([key, salt]) =>
            crypto.subtle
            .encrypt({ name: "AES-GCM", iv }, key, data)
            .then(ciphertext => `${window.crypter.buf2hex(salt)}-${window.crypter.buf2hex(iv)}-${window.crypter.buf2hex(ciphertext)}`),
        );
    },
    decrypt: (passphrase, saltIvCipherHex) => {
        const [salt, iv, data] = saltIvCipherHex.split("-").map(window.crypter.hex2buf);
        return window.crypter.deriveKey(passphrase, salt)
        .then(([key]) => crypto.subtle.decrypt({ name: "AES-GCM", iv }, key, data))
        .then(v => window.crypter.buf2str(new Uint8Array(v)));
    },
    deriveKey: (passphrase, salt) => {
        salt = salt || crypto.getRandomValues(new Uint8Array(8));
        return crypto.subtle
        .importKey("raw", window.crypter.str2buf(passphrase), "PBKDF2", false, ["deriveKey"])
        .then(key =>
            crypto.subtle.deriveKey(
                { name: "PBKDF2", salt, iterations: 50000, hash: "SHA-512" },
                key,
                { name: "AES-GCM", length: 256 },
                false,
                ["encrypt", "decrypt"],
            ),
        )
        .then(key => [key, salt]);
    },
    str2buf: (str) => {
        return new TextEncoder("utf-8").encode(str);
    },
    buf2str: (buffer) => {
        return new TextDecoder("utf-8").decode(buffer);
    },
    hex2buf: (hexStr) => {
        return new Uint8Array(hexStr.match(/.{2}/g).map(h => parseInt(h, 16)));
    },
    buf2hex: (buffer) => {
        return Array.prototype.slice
        .call(new Uint8Array(buffer))
        .map(x => [x >> 4, x & 15])
        .map(ab => ab.map(x => x.toString(16)).join(""))
        .join("");
    }
};

window.srp = {
  defaults: {
    N: "94ce8df220e993be63d9f7a31d566fa5f840e9d146e61fc4ec17e335ef6ebb4b502946459b4ab6c72e309218a4ff6be2050c0aa5d41f61c7f849c024e81ddc224ef51f2caedd1f331584e62673afd87288692cf4cdaa487ce2a55517ab3674ddf664d8c89993ca5b0ae81cd5e36099d8c279dcde7f1ccf9b5c810384c116ca31ee904d87ce32e68159628ff123220326fde409c7c1f59283db1a596456a424322945c055416c045efcadf25e742027f499af5c0871d2189515409f8d7270e2e74d14f943296ce0ef44c94e1366285d6a41a69a9ef2011af87f7654c2b7d2346b9858f186eaa868e85d64266596c7a7fe5993843ecba013e2530c48387414de1f",
    g: "2",
    n: 0,
  },
  register: function(username, password) {
    this.defaults.n = str2bigInt(this.defaults.N, 16);
    let s = this.randomSeed(256);
    let x = this.generateX(s, username, password);
    let v = this.generateWithPrime(x);

    let registerData = {
        username: username,
        salt: s,
        verifier: v,
    };

    axios.post('/register', registerData).then(function (response) {
      console.log(response.data);
    });
  },
  login: function(username, password){
    this.defaults.n = str2bigInt(this.defaults.N, 16);
    let a = this.randomSeed(256);
    let A = this.generateWithPrime(a);

      // window.crypter.encrypt("hello", "world")
      // .then(encrypted => console.log("ENCRYPTED", encrypted) || encrypted)
      // .then(encrypted => window.crypter.decrypt("hello", encrypted))
      // .then(decrypted => console.log("DECRYPTED ", decrypted) || decrypted);


    axios.post('/login', {
      phase: 1,
      I: username,
      A: A
    }).then((response) => {
      if(response.data.status === true){
        this.loginP2(username, password, A, a, response.data.B, response.data.s);
      }
    });
  },
  loginP2: function(username, password, A, a, B, s){
    let x = this.generateX(s, username, password);
    let S = this.generateS(A, B, a, x);
    let K = this.hash(S);
    let M = this.hash(K + A + B + username + s + this.defaults.N + this.defaults.g);

    axios.post('/login', {
      phase: 2,
      M: M,
    }).then((response) => {
      if(response.data.status === true && response.data.Z !== null) {
        let Z = this.hash(A + M + K);

        if(response.data.Z === Z){
          console.log('Verified');
        }
      }
    });
  },
  generateX(s, username, password){
    return this.hash(s + this.hash(username + ":" + password));
  },
  generateS: function(A, B, a, x, u) {
    let k = str2bigInt(this.hash(this.defaults.N + this.defaults.g), 16);
    let y = str2bigInt(x, 16);

    u = str2bigInt(this.hash(A + B), 16);
    B = str2bigInt(B, 16);
    a = str2bigInt(a, 16);

    let S = powMod(sub(B, mult(k, powMod(this.defaults.g, y, this.defaults.n))), add(a, mult(u, y)), this.defaults.n);

    return bigInt2str(S, 16).toLowerCase();
  },
  generateWithPrime: function(y) {
    y = str2bigInt(y, 16);
    return bigInt2str(powMod(this.defaults.g, y, this.defaults.n), 16).toLowerCase();
  },
  randomSeed: function(size){
      return crypt.randomBytes(size).toString('hex');
  },
  hash: function(content){
      return crypt.createHash("sha256").update(content, "ascii").digest("hex");
  },
};


